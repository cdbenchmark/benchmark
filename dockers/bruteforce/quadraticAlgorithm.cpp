/*
 *      This program reads positions from a folder with binary files (<path>/posN.bin),
 *      where each file defines a list of spheres as a double array: [x y z radius x y z radius x y z radius ... ].
 *      For every set of positions, the contacts between spheres are detected using a naive all-vs-all algorithm,
 *      and (optionaly) they are listed in an output text file.
 * 
 *       Compilation :                               g++ -std=c++17 quadraticAlgorithm.cpp -o bruteForce
 *       Execution without writing files:            ./bruteForce <folder>
 *       Execution with writing files (much longer): ./bruteForce dump <folder>
*/

#include "../../helpers/binaryIO.hpp"
#include <cmath>
#include <chrono>

using namespace std;

// A function checking all spheres vs. all spheres (order N²)
vector<int> findInteractions(vector<double> spheres)
{
	int numSpheresTimes4 = spheres.size();
	auto intrs = vector<int>();
	for (int b1 = 0; b1<numSpheresTimes4; b1+=4)
	{
		for (int b2 = 0; b2<numSpheresTimes4; b2+=4)
		{
			if (b1 == b2) continue;
			// A. True contact, compute dx²+dy²+dz² - (rad1+rad2)²
            double squaredDist = pow(spheres[b1]-spheres[b2],2)+pow(spheres[b1+1]-spheres[b2+1],2)+pow(spheres[b1+2]-spheres[b2+2],2) - pow(spheres[b1+3]+spheres[b2+3],2);
            if (squaredDist<0) {intrs.push_back(b1/4); intrs.push_back(b2/4);}
            // B. Check bounding boxes only
// 			double cumSizes = spheres[b1+3]+spheres[b2+3];
// 			bool noOverlap =  	(abs(spheres[b1]-spheres[b2]) >  cumSizes) or
// 								(abs(spheres[b1+1]-spheres[b2+1]) >  cumSizes) or
// 								(abs(spheres[b1+2]-spheres[b2+2]) >  cumSizes);
// 			if (not noOverlap) {intrs.push_back(b1/4); intrs.push_back(b2/4);}
		}
	}
	return intrs;
}

// Write interactions to file
void dumpInteractions(vector<vector<int> > intrsAll)
{
    for (size_t f = 0; f < intrsAll.size(); f++) {
        vector<int>& intrs = intrsAll[f];
        string filename = "intrs"+std::to_string(f)+".txt";
        std::ofstream outputFile(filename);
        for (size_t i = 0; i < intrs.size(); i += 2) {
            outputFile <<1 <<" "<< intrs[i] << ' '<<intrs[i + 1];
            if (i+2 < intrs.size()) outputFile << '\n';
        }
        outputFile.close();
    }
}



int main(int argc, char* argv[]) {
    auto startTime = std::chrono::high_resolution_clock::now();
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <folder_path>\nor with dumping interactions to file:  "<< argv[0] << " dump <folder_path>"<< std::endl;
        return 1;
    }
    std::string folderPath = (argc==2) ? argv[1] : argv[2];    
    
    
    // get positions from all files as a 2D array, nth column is {x, y, z, rad, x, y, z, rad, ...} at iteration n
    vector<vector<double> > positions = readAllpositions(folderPath);
    // prepare some timings
    auto readInT = std::chrono::high_resolution_clock::now();
    
    // generate interactions as a 2D array, nth column is {id1, id2, z, rad, x, y, z, rad, ...} at iteration n
    auto intrsAll = vector<vector<int> >();
    for (const auto& pos : positions)
    {
        intrsAll.push_back(findInteractions(pos));
    }
    auto colliderT = std::chrono::high_resolution_clock::now();
    if (argc>2) dumpInteractions(intrsAll);
    auto dumpT = std::chrono::high_resolution_clock::now();
    
    auto readTime = std::chrono::duration_cast<std::chrono::microseconds>(readInT - startTime);
    auto colliderTime = std::chrono::duration_cast<std::chrono::microseconds>(colliderT - readInT);
    auto writeTime = std::chrono::duration_cast<std::chrono::microseconds>(dumpT - colliderT);
    
    std::cout << "read files: " << readTime.count()/1e6 << " s" << std::endl;
    std::cout << "collision detection: " << colliderTime.count()/1e6 << " s" << std::endl;
    if (argc>2) std::cout << "write files: " << writeTime.count()/1e6 << " s" << std::endl;
    
    return 0;
}

