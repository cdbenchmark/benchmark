apt-get update && apt-get install -y --no-install-recommends -y build-essential g++
g++ -std=c++17 -O3 dockers/bruteforce/quadraticAlgorithm.cpp -o bruteforce
mkdir -p install
mv bruteforce install/bruteforce
ls install
apt-get remove -y build-essential g++
