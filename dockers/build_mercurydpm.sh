apt-get update && apt-get install -y build-essential g++ gfortran git cmake python3
cd /tmp
git clone https://bitbucket.org/mercurydpm/mercurydpm.git
mkdir -p build && cd build
cmake -DMercury_Include_Xballs_Support=OFF -DMercury_USE_OpenMP=ON -DMercury_BUILD_DOCUMENTATION=OFF ../mercurydpm
cd Drivers/Papers/OSBenchmark/
make -j4
mkdir -p /install
mv ./* /install/
rm -rf /tmp/* #clean to reduce image size
apt-get remove -y g++ gfortran git cmake # python?
