num_spheres = 2000 if len(sys.argv)<2 else int(sys.argv[1])
tolerance = 1e-3

O.materials.append(FrictMat(young=1.0e7, poisson=0.2, density=2500, label='M1'))

O.engines = [
		ForceResetter(),
		InsertionSortCollider([
				Bo1_Sphere_Aabb(),
				Bo1_Box_Aabb(),
		], verletDist=-0.2),
		InteractionLoop(
				[Ig2_Sphere_Sphere_ScGeom(interactionDetectionFactor=1.01), Ig2_Box_Sphere_ScGeom(interactionDetectionFactor=1.01)],
				[Ip2_FrictMat_FrictMat_FrictPhys()],
				[Law2_ScGeom_FrictPhys_CundallStrack(label='law')]
		),
		GlobalStiffnessTimeStepper(),
		NewtonIntegrator(damping=0.2, gravity=[0,-9.81,0])
]

from yade import pack
sp = pack.SpherePack()
sp.makeCloud((-0.6,0,-0.6), (0.6,3,0.6), -1, 0.3333, num_spheres, False, 0.85, seed=1)
O.bodies.append([sphere(center, rad) for center, rad in sp])

walls = aabbWalls([(-1,0,-1), (1,5,1)], thickness=0)
wallIds = O.bodies.append(walls)

O.dt = 0.5*PWaveTimeStep()

from yade import plot
O.engines=O.engines+[PyRunner(iterPeriod=20,command='plot.addData(t=O.time,Ek=kineticEnergy(),Ee=law.elasticEnergy(),r=kineticEnergy()/(max(law.elasticEnergy(),1e-10)))',label='recorder')]
plot.plots={'t':('Ek','Ee',None,'r')}
plot.plot()

os.system("mkdir -p out/A")
os.system("mkdir -p out/B")

def recordPosTxt(filename):
	spO = pack.SpherePack()
	spO.fromSimulation()
	spO.save(str(filename)+".txt")

def recordPosBinary(filename):
	from array import array
	f=open(str(filename)+".bin",'wb')
	float_list = []
	for b in O.bodies:
		if not isinstance(b.shape,Sphere): continue
		float_list.extend(b.state.pos)
		float_list.append(b.shape.radius)
	array('d', float_list).tofile(f)
	f.close()

from yade import timing
O.timingEnabled=True

while O.time<0.5:
	O.run(100,True)
law.neverErase=True
for nIter in range(400):
	recordPosTxt("out/A/pos"+str(nIter))
	recordPosBinary("out/A/bpos"+str(nIter))
	O.step()
law.neverErase=False

while O.time<2.5:
	O.run(100,True)
law.neverErase=True
for nIter in range(100):
	recordPosTxt("out/B/pos"+str(nIter))
	recordPosBinary("out/B/bpos"+str(nIter))
	O.step()
law.neverErase=False
