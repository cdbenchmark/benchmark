#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <filesystem>

// NOTE: filesystem needs to be compiled with "g++ -std=c++17"

namespace fs = std::filesystem;

std::vector<double> readBinaryFileToVector(const std::string& filename) {
    // Open the binary file in binary mode
    std::ifstream file(filename, std::ios::binary);

    if (!file.is_open()) {
        std::cout << "Error opening file: " << filename << std::endl;
        return std::vector<double>();
    }
	file.seekg(0, std::ios::end);
	int fileSize = file.tellg();
	file.seekg(0, std::ios::beg);
	int count = fileSize / sizeof(double);
	std::vector<double> doubleVector(count);
	file.read(reinterpret_cast<char*>(&doubleVector[0]), fileSize);
	file.close();
    return doubleVector;
}


std::vector<std::vector<double> > readAllpositions(std::string folderPath)
{
    if (!fs::exists(folderPath) || !fs::is_directory(folderPath)) {
        std::cerr << "Error: Folder does not exist or is not a directory." << std::endl;
        return std::vector<std::vector<double> >();
    }
    std::vector<std::vector<double> >  positions;
    int i;  
    // collect positions
    for (i = 0; ; ++i) {
        fs::path filePath = folderPath + "/bpos" + std::to_string(i) + ".bin";
        if (!fs::exists(filePath)) break;
        positions.push_back(readBinaryFileToVector(filePath));
    }
    std::cout << i-1 << " snapshots scanned " <<std::endl;
    return positions;
}
