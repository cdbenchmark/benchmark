import os, sys
import numpy as np

pathA = sys.argv[1]
pathB = sys.argv[2]

def compareInts(ref,res):
	refReal=[(int(x[1]),int(x[2])) for x in ref if x[0]>0]
	resReal=[(min(x[1],x[2]),max(x[1],x[2])) for x in res if x[0]==1]
	
	resReal=sorted(resReal)
	missing = sorted(set(refReal).difference(set(resReal)))
	excess = sorted(set(resReal).difference(set(refReal)))
	return missing,excess,len(ref)

os.listdir(pathA)
i=0
errors = 0
missingFiles = 0
while os.path.exists(pathA+"/intrs"+str(i)+".txt"):
	if not os.path.exists(pathB+"/intrs"+str(i)+".txt"):
		print("missing output file: ",pathB+"/intrs"+str(i)+".txt")
		missingFiles+=1
		i+=1
		continue
	ref=np.loadtxt(pathA+"/intrs"+str(i)+".txt",dtype=float)
	res=np.loadtxt(pathB+"/intrs"+str(i)+".txt",dtype=int)
	missing,excess,total = compareInts(ref,res)
	if len(missing)!=0 or len(excess)!=0:
		print("intrs"+str(i)+".txt missing", len(missing),", and in excess of ", len(excess)," (total ",len(ref),")")
		if len(missing)<10: print(missing)
		else: print(missing[:10]," ...")
		errors+= (len(missing)+len(excess))
	i+=1

if not errors and not missingFiles:
	print("OK")
