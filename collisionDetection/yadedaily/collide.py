t0 = time.time()

if len(sys.argv)<2:
	print("please provide path to snapshots files ('path/to/file' if the actual numbered files are 'path/to/fileN'")
	exit()
inputFolder = sys.argv[1]

outputFolder = "output/"+inputFolder
if not os.path.exists(outputFolder): os.system("mkdir -p "+outputFolder)

positions=[]
numFiles=0
from yade import pack
while os.path.exists(inputFolder+"/pos"+str(numFiles)+".txt"):
	positions.append(pack.SpherePack())
	positions[-1].load(inputFolder+"/pos"+str(numFiles)+".txt")
	numFiles+=1
	#positions.append(numpy.fromfile("inputFolder"+str(num)+".txt",sep=" "))
print(numFiles,"snapshots loaded")

if numFiles==0:
	print("no files found")
	exit()

numSpheres = len(positions[0])
positions[0].toSimulation()
for b in O.bodies:
	b.state.blockedDOFs="xyzXYZ"
	
walls = aabbWalls([(-1,0,-1), (1,5,1)], thickness=0)
wallIds = O.bodies.append(walls)

O.dt = 1

O.materials.append(FrictMat(young=1.0e7, poisson=0.2, density=2500, label='M1'))
O.engines = [
		ForceResetter(),
		InsertionSortCollider([
				Bo1_Sphere_Aabb(),
				Bo1_Box_Aabb(),
		], verletDist=-0.2),
		InteractionLoop(
				[Ig2_Sphere_Sphere_ScGeom(), Ig2_Box_Sphere_ScGeom()],
				[Ip2_FrictMat_FrictMat_FrictPhys()],
				[Law2_ScGeom_FrictPhys_CundallStrack(label='law')]
		),
		PyRunner(iterPeriod=1,command='updateVelocity(int(O.time)+1)',initRun=True,label='mover'),
		PyRunner(iterPeriod=1,command='recordIntrs(int(O.time),outputFolder+"/intrs")',initRun=True,dead=False,label='dumper'),
		NewtonIntegrator(damping=0.2, gravity=[0,-9.81,0])
]

def updateVelocity(n):
	if n>=numFiles: return
	for b in O.bodies:
		if b.id>=numSpheres: break
		b.state.vel=positions[n][b.id][0]-b.state.pos



def recordIntrs(nIter,pathName,sort=True):
	f=open(pathName+str(nIter)+".txt",'w')
	print("write results to", pathName+str(nIter)+".txt")
	allInts=[]
	for i in O.interactions.all():
			(id1,id2) = (i.id1,i.id2) if i.id1<i.id2 else (i.id2,i.id1)
			allInts.append((int(i.isReal),id1,id2))
	if sort: allInts = sorted(allInts)
	for real,id1,id2 in allInts:            
		f.write(str(real)+" "+str(id1)+" "+str(id2)+"\n")
	f.close()

O.timingEnabled=True
from yade import timing
t1 = time.time()

O.step()
t2 =  time.time()
O.run(numFiles-1,True)
t3 =  time.time()

print("prepare scene: ",t1-t0,"s")
print("collision detection (first iter): ",t2-t1,"s")
print("collision detection (all iters): ",t3-t2,"s")
timing.stats()
