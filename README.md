# ColliderBenchmark

Benchmark of collision detection algorithms and open-source implementations

## Contributions

Contributions require **A**/ some source code, **B**/ a build process, and **C**/ an executable to analyse positions given in binary format. A "contribution" is assigned a unique _codename_ (lower-cased only), for instance "_mercurydpm_".

Note: Each time _codename_ appears in the explanations below, it needs to be replaced by a specific name.

### A. Source code

- preferably a git repository that will be later included as a submodule.
- The codename needs to be declared in a section of [docker/builds.yml](https://gricad-gitlab.univ-grenoble-alpes.fr/cdbenchmark/benchmark/-/blob/main/dockers/builds.yml#L42).

### B. Build

Please provide a bash script called _build\_codename\_.sh_ (where codename is replaced by the specific name) to compile the code and add it to folder _dockers/_ (example [here](https://gricad-gitlab.univ-grenoble-alpes.fr/cdbenchmark/benchmark/-/blob/main/dockers/build_mercurydpm.sh)). 
- It is safe to assume that the source code is in directory _codename_ when the script is executed.
- The compiled code and everything needed for future execution needs to be [moved into /_install_ folder](https://gricad-gitlab.univ-grenoble-alpes.fr/cdbenchmark/benchmark/-/blob/main/dockers/build_mercurydpm.sh#L8)  (note the "/", this is root path), it needs to be done by the build script.
- Please try to remove non-necessary files at the end of the build (source code and temporary object files) to minimize the size of the docker images.

### C. Executable

- The name of the program for the analysis phase is declared in [/.gitlab-ci.yml](https://gricad-gitlab.univ-grenoble-alpes.fr/cdbenchmark/benchmark/-/blob/4f9c14f79f90b6e7afffc87fc92459b018ebf05c/.gitlab-ci.yml#L102) (_codename_ is a valid name and is suggested)

#### Input
- In input, the program needs to accept a folder name, e.g.:
```bash
codename /path/to/folder
```
- The folder contains a number of files (binary and text formats are available, preferably use the binary) giving positions and sizes of spheres (and facets) at different times.

- The content of each file is _{x y z rad x y z rad ...}_
There is a helper function to load all files in a _vector<vector\<double\> >_ in [/helpers/binaryIO.hpp](https://gricad-gitlab.univ-grenoble-alpes.fr/cdbenchmark/benchmark/-/blob/f3b26a8467729c104ebec21f9a6ff50a77bc20e2/helpers/binaryIO.hpp#L30). It is used for instance in [bruteForce.cpp](https://gricad-gitlab.univ-grenoble-alpes.fr/cdbenchmark/benchmark/-/blob/f3b26a8467729c104ebec21f9a6ff50a77bc20e2/dockers/bruteforce/quadraticAlgorithm.cpp#L69).

TODO: define a file format for facets, if needed

- Dumping the lists of interactions to disk should be done optionally (see next section), i.e. the program should accept an additional parameter:
```bash
codename /path/to/folder      # no output to disk
codename /path/to/folder dump # write interactions to files
```
Alternatively (but not preferably), two different flavors of "the program" can be used:
```bash
codename1 /path/to/folder      # no output to disk
codename2 /path/to/folder      # write interactions to files
```

#### Output

- The expected output, if requested, is one file listing all interactions for every timestep. If the input folder contains _\{pos1.bin, pos2.bin,..., posN.bin\}_, then after execution current path (where the progam is executed) should contain _\{intrs1.txt, intrs2.txt,..., intrsN.txt\}_.

- The format of the output file is _\{1 id1 id2 1 id1 id2 ... \}_ (Note that "1" is repeated for no apparent reason, we may use it as a boolean at some point, else we will remove it). For now please repeat the "1" for compatibility.

- Additionaly, the program should record time spent on collision detection at each iteration using internal timing, and it should write the time spent at every iteration in one single file (_./timings.txt_) containing _N_ values (if there are _N_ input files).

#### Workflow

- Provided that the program accepts input and generates output like above, the contribution is complete. 

- As part of the pipeline, each program will be called repeatedly on various inputs, with and without dumping to disk (the later will be used for walltime measurement).

- Downstream in the pipeline the lists of interactions will be cross-checked vs. reference data and timings will be aggregated and plotted. 
